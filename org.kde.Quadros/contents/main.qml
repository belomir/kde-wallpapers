/*
 * Copyleft by Sergey Roganov, 2013
 * qml animated version of Quadros wallpaper
 * distribute under terms of GNU/GPL v.3+
 */

import QtQuick 1.0

Rectangle {
	id: main
	width: 640
	height: 480
	color: Qt.darker("lightblue", 1.25) // default color
	clip: true // crop the image by this rect
	property int rows: 3+Math.random()*2 // how many rows
	property int cols: rows*width/height + 1 // columns are comuted accrding to width of quadros
	property int pause: 500 // some pausing in animation. more means slower
	
	signal refresh() // empty signal for connections
	signal newColor()
	signal reset()
	
	onNewColor: {
		main.color = Qt.hsla(Math.random(), Math.random(), Math.random()*0.625+0.125, 1)
	}
	onReset: {
		main.color = Qt.darker("lightblue", 1.25)
	}
	
	// main color is not displayed, so this is appendix
	Behavior on color { ColorAnimation { duration: 50; easing.type: Easing.InOutCubic } }
	
	MouseArea {
		anchors.fill: parent
		onPressAndHold: {
			newColor();
		}
		onReleased: {
			refresh();
		}
		onDoubleClicked: {
			reset()
		}
	}
	
	Timer {
		running: true
		repeat: true
		interval: 1000*60*10
		triggeredOnStart: true
		onTriggered: {
			refresh();
		}
	}
	
	Timer {
		running: true
		repeat: true
		interval: 1000*60*60
		triggeredOnStart: false
		onTriggered: {
			newColor();
		}
	}
	
	// grid of quadros
	Grid {
		anchors.centerIn: parent
		rows: main.rows
		columns: main.cols
		Repeater {
			model: main.rows*main.cols
			delegate: Rectangle {
				id: index // need to read properties
				z: 0 // default position
				height: main.height/main.rows+1 // delete the empty line above the grid
				width: height
				color: main.color // not displayed, need by childs
				smooth: true
				
				// make gradient rotation possible: insert rect into clipped rect
				Rectangle {
					anchors.fill: parent
					color: "transparent" // need this rect just to crop rotated gradient
					clip: true // cropping
					Rectangle { // rotated rect with gradient
						z: index.z
						anchors.centerIn: parent
						width: parent.width*2 // too lazy to count radius
						height: width
						rotation: 360*Math.random()
						smooth: true
						LinearGradient {
							anchors.fill: parent
							start: Qt.point(0, 0)
							end: Qt.point(0, 300)
							gradient: Gradient {
								GradientStop { position: 0.0; color: "white" }
								GradientStop { position: 1.0; color: "black" }
							}
						}
					}
				}
				
				// shadow (took example somewhere on internet qt/qml forum)
				BorderImage {
					id: shadow
					z: -1 // make the shadow being under the rect
					anchors.fill: parent
					rotation: 0
					opacity: 0 // initial values
					anchors { leftMargin: -20; topMargin: -20; rightMargin: -25; bottomMargin: -25 } // values are experienced
					border { left: 25; top: 25; right: 25; bottom: 25 } // values are experienced
					source: "shadow.png" // png created in Gimp
					smooth: true
					// little hack to make changing of the order (z) invisible
					Behavior on opacity {
						SequentialAnimation {
							NumberAnimation { // go to full transparent
								id: animstop
								duration: main.pause
								easing.type: Easing.InOutQuad
								to: 0
							}
							PauseAnimation { duration: main.pause/100 } // wait for new order
							NumberAnimation { // go to new opacity value
								duration: 1000+500*Math.random()
								easing.type: Easing.InOutQuad
							}
						} // with InOutCubic seems like too big pause
					}
					Behavior on rotation { NumberAnimation { duration: 1500; easing.type: Easing.InOutCubic } }
				}
				
				Behavior on z { 
					SequentialAnimation { // one hack more
						PauseAnimation { duration: main.pause } // waiting for transparent shadows
						NumberAnimation { duration: main.pause/100 } // changing order
					}
				}
				
				signal refresh() // full changes
				signal ping() // single rect refresh
				onPing: {
					color = Qt.lighter(main.color, 1+Math.random()*0.5)
					start.color = Qt.darker(index.color, 1+0.5*Math.random())
					stop.color = Qt.lighter(index.color, 1+0.5*Math.random())
					shadow.rotation = 6-8*Math.random() // don't use big values
					shadow.opacity = Math.random()
				}
				onRefresh: {
					color = Qt.lighter(main.color, 1+Math.random()*0.5)
					start.color = Qt.darker(index.color, 1+0.5*Math.random())
					stop.color = Qt.lighter(index.color, 1+0.5*Math.random())
					// sometimes need "shadow.opacity = 0" here
					z = 1+5*Math.random()
					shadow.rotation = 6-8*Math.random()
					shadow.opacity = Math.random()
				}
				Connections {
					target: main
					onRefresh: refresh()
					onReset: reset()
				}
				
				Timer { // make little single changes
					running: true
					repeat: true
					interval: 1000*45*(1+9*Math.random())
					triggeredOnStart: false
					onTriggered: {
						ping();
						interval = 1000*45*(1+9*Math.random()); // at random time each step
					}
				}
				
				MouseArea { // single refresh is clickable
					anchors.centerIn: parent
					width: parent.width/4
					height: parent.height/4
					onClicked: {
						ping();
					}
				}
			}
		}
	}
} 
