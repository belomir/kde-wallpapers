/*
 * Copyleft by Sergey Roganov, 2013
 * Ariya static wallpaper port to qml
 * distribute under terms of GNU/GPL v.3+
 */

import QtQuick 1.0

Rectangle {
	id: main
	width: 640
	height: 480
	clip: true
	color: "gray" // inintial color
	
	property int lines: 20+Math.random()*20 // number of lines
	property real koef: 0.05*(main.width>main.height?main.width:main.height) // some moving behind corner to "hide" rays center
	
	rotation: 180
	
	signal refresh() // connected to other objects
	signal newColor()
	
	transform: Rotation { id: rot; origin.x: width/2; origin.y: height/2; axis {x: 0; y: 1; z: 0} angle: 0} // mirror by middle y axis
	
	onNewColor: {
		main.color = Qt.hsla(Math.random(), 0.125+Math.random()*0.5, Math.random()*0.5+0.125, 1)
	}
	
	Behavior on color { ColorAnimation { duration: 500; easing.type: Easing.InOutCubic } }
	
	MouseArea {
		anchors.fill: parent
		onPressAndHold: {
			newColor();
		}
		onReleased: {
			refresh();
		}
		// reset to default
		onDoubleClicked: {
			main.color = "gray"
			main.rotation = Math.floor(Math.random()*2)*180
			rot.angle = 180*Math.floor(Math.random()*2)
		}
	}
	
	// rotating by timer
	Timer {
		running: true
		repeat: true
		interval: 1000*60*10
		triggeredOnStart: true
		onTriggered: {
			refresh();
		}
	}
	
	// change color by bigger timer
	Timer {
		running: true
		repeat: true
		interval: 1000*60*60
		triggeredOnStart: false
		onTriggered: {
			newColor();
		}
	}
	
	// rays item
	Item {
		id: rays
		
		x: -main.koef
		y: -main.koef // it moved behind corner
		
		width: main.width+main.koef
		height: main.height+main.koef // size is incresed by movement behind corner value
		
		// connected signal
		signal refresh()
		
		onRefresh: {
			rotation = Math.random()*90
		}
		
		Connections {
			target: main
			onRefresh: rays.refresh() // !!! this doesn't work without "rays."
		}
		
		// rotation rays
		transformOrigin: Item.TopLeft
		rotation: 0
		Behavior on rotation { NumberAnimation { duration: 2500; easing.type: Easing.OutElastic } }
		
		// rays implemented by rotated rectangles
		Repeater {
			model: main.lines
			anchors.fill: parent
			delegate: Rectangle {
				
				signal ping() // update separate "ray"
				onPing: {
					color = Qt.lighter(main.color, 1+Math.random()*1)
				}
				
				anchors.left: parent.left
				anchors.top: parent.top
				x: -main.koef
				y: -main.koef
				// rays will be distributed on circle
				width: Math.PI*height/main.lines // arc part
				height: Math.sqrt(Math.pow(main.width,2)+Math.pow(main.height,2))+2*main.koef // radius = screen diag
				
				transform: Rotation { origin.x: 0; origin.y: 0; angle: -180/Math.PI*Math.PI*height/main.lines/height*(index-0.1+Math.random()*0.1)} // angle is a part of halfcircle computed according to each rectangle width
				
				smooth: true
				
				color: Qt.lighter(main.color, 1+Math.random()*1)
				Behavior on color { ColorAnimation { duration: 1500; easing.type: Easing.InOutCubic } }
				
				// update each ray at random time
				Timer {
					running: true
					repeat: true
					interval: 1000*30*(1+9*Math.random())
					triggeredOnStart: false
					onTriggered: {
						ping()
					}
				}
			}
		}
	}
	
	// film grain is tiled image cover
	Image {
		id: grain
		anchors.centerIn: parent
		width: Math.sqrt(Math.pow(main.width,2)+Math.pow(main.height,2))+main.koef // radius
		height: width
		source: "filmgrain.jpg"
		fillMode: Image.Tile
		opacity: 0.05 // initial opacity
		
		// refreshing is connected as rays
		signal refresh()
		onRefresh: {
			opacity = 0.05+Math.random()*0.1
			scale = 1+Math.random()
			rotation = 30*Math.random()-15
		}
		Connections {
			target: main
			onRefresh: grain.refresh() // !!! don't forget id
		}
		
		smooth: true
		Behavior on opacity { NumberAnimation { duration: 1500; easing.type: Easing.InOutCubic } }
		Behavior on scale { NumberAnimation { duration: 1500; easing.type: Easing.InOutCubic } }
		Behavior on rotation { NumberAnimation { duration: 1500; easing.type: Easing.InOutCubic } }
	}
	
	// overrided gradients
	Item {
		anchors.centerIn: parent
		width: Math.sqrt(Math.pow(main.width,2)+Math.pow(main.height,2))+main.koef
		height: width
		id: patina
		// one gradient per side, rotated randmly
		Repeater {
			model: 4
			delegate : Rectangle {
				// refresh by main
				signal refresh()
				onRefresh: {
					pos.position = 0.25+Math.random()*0.4
					rotation = -15+30*Math.random()+index*90 // 4 sides
				}
				Connections {
					target: main
					onRefresh: refresh()
				}
				Behavior on rotation { NumberAnimation { duration: 500; easing.type: Easing.InOutCubic } }
				anchors.fill: parent
				smooth: true
				rotation: -15+30*Math.random()+index*90 // initial rotation
				gradient: Gradient {
					GradientStop { // dark side
						position: 0.0
						color: Qt.hsla(0,0,0,0.6)
					}
					GradientStop { // light side
						id: pos
						position: 0.25+Math.random()*0.4
						color: Qt.hsla(0,0,1,0.1)
						Behavior on position { NumberAnimation { duration: 500; easing.type: Easing.InOutCubic } }
					}
				}
			}
		}
	}
}