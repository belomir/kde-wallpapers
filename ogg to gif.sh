#!/bin/bash

echo "video to gif converter"

function usage
{
	echo -e "usage: "$0" [options] file"
	echo -e "  options: "
	echo -e "    -d | --duration            <seconds>    // 6  is default"
	echo -e "    -o | --offset              <seconds>    // 0  is default"
	echo -e "    -r | --rate | --framerate  <framerate>  // 15 is default"
	echo -e "    -q | --quality             <level>      // 10 is default"
	echo -e "    -f | --file                <file>       // variant to set file"
}

echo "preparation:"

file=""
tempdir="temp"
outdir="output"

duration=6 #secs
quality=10
framerate=15

# get options and parameters
while [ "$1" != "" ]; do
	case $1 in
		-f | --file )
			shift
			file="$1"
			;;
		-d | --duration )
			shift
			duration=$1
			;;
		-o | --offset )
			shift
			offset=$1
			;;
		-r | --rate | --framerate )
			shift
			framerate=$1
			;;
		-q | --quality )
			shift
			quality=$1
			;;
		-h | --help )
			usage
			exit
			;;
		* )
			if [ -z "$file" ]
			then
				file="$1"
			else
				echo "unknown extra option $1"
				usage
				exit 1
			fi
	esac
	shift
done

if [ -z "$file" ]
then
	echo "error: filename is not specified"
	usage
	exit 1
fi

path=$(dirname "$file")
filename=$(basename "$file")
name="${filename%.*}"
if [[ "$filename"=="$name" ]]
then
	extension=""
else
	extension="${filename##*.}"
fi

echo -e "  file:       $file"
echo -e "  filename:   $filename"
echo -e "  name:       $name"
echo -e "  extension:  $extension"
echo -e "  path:       $path"
echo -e "  duration:   $duration"
echo -e "  offset:     $offset"
echo -e "  framerate:  $framerate"
echo -e "  quality:    $quality"

cd "$path"

mkdir "$tempdir"

# make a little (few seconds) video from original
echo "making short video..."
avconv -i "$filename" -t $duration -q $quality -r $framerate -ss $offset "$tempdir/$filename"

# make set of pictures from video
echo "making set of pictures..."
mplayer -ao null "$tempdir/$filename" -vo jpeg:outdir="$tempdir/$outdir"

# make gif from the set of pictures
echo "making gif..."
convert "$tempdir/$outdir/*" "$name.gif"

# remove temporary files
echo "removing temporary files..."
rm -r "$tempdir"

cd "-" > /dev/null

echo "DONE"
