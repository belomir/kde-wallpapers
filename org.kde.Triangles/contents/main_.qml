/*
 * Copyleft by Sergey Roganov, 2013
 * qml animated version of Triangles wallpaper
 * distribute under terms of GNU/GPL v.3+
 */

import QtQuick 1.0
import Shapes 1.0

Rectangle {
	id: main
	width: 640
	height: 480
	color: Qt.darker("lightblue", 1.25) // default color
	clip: true // crop the image by this rect
	property int rows: 2+Math.random()*2 // how many rows
	property int cols: rows*width/height + 1 // columns are comuted accrding to width of quadros
	property int pause: 500 // some pausing in animation. more means slower
	
	signal refresh() // empty signal for connections
	signal newColor()
	signal reset()
	
	onNewColor: {
		main.color = Qt.hsla(Math.random(), Math.random(), Math.random()*0.625+0.125, 1)
	}
	onReset: {
		main.color = Qt.darker("lightblue", 1.25)
	}
	
	// main color is not displayed, so this is appendix
	Behavior on color { ColorAnimation { duration: 50; easing.type: Easing.InOutCubic } }
	
	MouseArea {
		anchors.fill: parent
		onPressAndHold: {
			newColor();
		}
		onReleased: {
			refresh();
		}
		onDoubleClicked: {
			reset()
		}
	}
	
	Timer {
		running: true
		repeat: true
		interval: 1000*60*10
		triggeredOnStart: true
		onTriggered: {
			refresh();
		}
	}
	
	Timer {
		running: true
		repeat: true
		interval: 1000*60*60
		triggeredOnStart: false
		onTriggered: {
			newColor();
		}
	}
	
	Rectangle {
		width: 200
		height: 150
		color: "red"
		border.width: 4
		border.color: "black"
	}
	
} 
