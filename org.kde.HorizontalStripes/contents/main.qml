/*
 * Copyleft by Sergey Roganov, 2015
 * HorizontalStripes qml animated wallpaper inspired by Quadros
 * distribute under terms of GNU/GPL v.3+
 */

import QtQuick 1.0

Rectangle {
	id: main
	width: 640
	height: 480
	color: Qt.darker("lightblue", 1.25) // default color
	clip: true // crop the image by this rect
	property int cols: 6+Math.random()*12 // number of colunms
	property int pause: 500 // some pausing in animation. more means slower
	
	signal refresh() // empty signal for connections
	signal newColor()
	signal reset()
	
	onNewColor: {
		main.color = Qt.hsla(Math.random(), Math.random(), Math.random()*0.625+0.125, 1)
	}
	onReset: {
		main.color = Qt.darker("lightblue", 1.25)
	}
	
	// main color/background
	Behavior on color { ColorAnimation { duration: 50; easing.type: Easing.InOutCubic } }
	
	MouseArea {
		anchors.fill: parent
		onPressAndHold: {
			newColor();
		}
		onReleased: {
			refresh();
		}
		onDoubleClicked: {
			reset()
		}
	}
	
	Timer {
		running: true
		repeat: true
		interval: 1000*60*10
		triggeredOnStart: true
		onTriggered: {
			refresh();
		}
	}
	
	Timer {
		running: true
		repeat: true
		interval: 1000*60*60
		triggeredOnStart: false
		onTriggered: {
			newColor();
		}
	}
	
	// Column of stripes
	Column {
		anchors.centerIn: parent
// 		move: Transition {
// 			NumberAnimation {
// 				properties: "x"
// 				easing.type: Easing.InOutCubic
// 				duration: main.pause
// 			}
// 		}
		Repeater {
			model: main.cols
			delegate: Rectangle {
				id: index // need to read properties
				z: 0 // default position
				height: main.height/main.cols*(Math.random()+1) // each stripe width is slightly bigger than width/cols height
				width: main.width // full
				color: main.color // not displayed, need by childs
				smooth: true
				
				// make gradient rotation possible: insert rect into clipped rect
				Rectangle {
					anchors.fill: parent
					color: "transparent" // need this rect just to crop rotated gradient
					clip: true // cropping
					Rectangle { // rotated rect with gradient
						z: index.z
						anchors.centerIn: parent
						width: parent.width*2 // too lazy to count radius
						height: parent.width*2
						rotation: 360*Math.random()
						smooth: true
						gradient: Gradient {
							GradientStop {
								id: start
								position: 0.0
								color: Qt.darker(index.color, 1.5) // from dark
								Behavior on color { 
									SequentialAnimation {
										//PauseAnimation { duration: main.pause } // appendix
										ColorAnimation { duration: main.pause*(1+Math.random()); easing.type: Easing.InOutCubic }
									}
								}
							}
							GradientStop {
								id: stop
								position: 1.0
								color: Qt.lighter(index.color, 1.5) // to light
								Behavior on color { 
									SequentialAnimation {
										//PauseAnimation { duration: main.pause } // appendix
										ColorAnimation { duration: main.pause*(1+Math.random()); easing.type: Easing.InOutCubic }
									}
								}
							}
						}
					}
				}
				
				// shadow (took example somewhere on internet qt/qml forum)
				BorderImage {
					id: shadow
					z: -1 // make the shadow being under the rect
					anchors.fill: parent
					rotation: 0
					opacity: 0 // initial values
					anchors { leftMargin: -20; topMargin: -20; rightMargin: -25; bottomMargin: -25 } // values are experienced
					border { left: 25; top: 25; right: 25; bottom: 25 } // values are experienced
					source: "shadow.png" // png created in Gimp
					smooth: true
					// little hack to make changing of the order (z) invisible
					Behavior on opacity {
						SequentialAnimation {
							NumberAnimation { // go to full transparent
								id: animstop
								duration: main.pause
								easing.type: Easing.InOutQuad
								to: 0
							}
							PauseAnimation { duration: main.pause/100 } // wait for new order
							NumberAnimation { // go to new opacity value
								duration: main.pause*(1+Math.random())
								easing.type: Easing.InOutQuad
							}
						} // with InOutCubic seems like too big pause
					}
					Behavior on rotation { NumberAnimation { duration: main.pause; easing.type: Easing.InOutCubic } }
				}
				
				Behavior on z { 
					SequentialAnimation { // one hack more
						PauseAnimation { duration: main.pause } // waiting for transparent shadows
						NumberAnimation { duration: main.pause/100 } // changing order
					}
				}
				
				Behavior on height { 
					SequentialAnimation { // one another hack
						//PauseAnimation { duration: main.pause } // waiting for transparent shadows
						NumberAnimation {
							duration: main.pause
							easing.type: Easing.InOutCubic
						} // changing order
					}
				}
				
				signal refresh() // full changes
				signal ping() // single rect refresh
				onPing: {
					color = Qt.lighter(main.color, 1+Math.random()*0.5)
					height = main.height/main.cols*(Math.random()+1)
					start.color = Qt.darker(index.color, 1+0.5*Math.random())
					stop.color = Qt.lighter(index.color, 1+0.5*Math.random())
					shadow.rotation = 2-4*Math.random() // don't use big values
					shadow.opacity = Math.random()
				}
				onRefresh: {
					color = Qt.lighter(main.color, 1+Math.random()*0.5)
					height = main.height/main.cols*(Math.random()+1)
					start.color = Qt.darker(index.color, 1+0.5*Math.random())
					stop.color = Qt.lighter(index.color, 1+0.5*Math.random())
					// sometimes need "shadow.opacity = 0" here
					z = 1+5*Math.random()
					shadow.rotation = 2-4*Math.random()
					shadow.opacity = Math.random()
				}
				Connections {
					target: main
					onRefresh: refresh()
					onReset: reset()
				}
				
				Timer { // make little single changes
					running: true
					repeat: true
					interval: 1000*45*(1+9*Math.random())
					triggeredOnStart: false
					onTriggered: {
						ping();
						interval = 1000*45*(1+9*Math.random()); // at random time each step
					}
				}
				
				MouseArea { // single refresh is clickable
					anchors.centerIn: parent
					width: parent.width/4
					height: parent.height/4
					onClicked: {
						ping();
					}
				}
			}
		}
	}
} 
