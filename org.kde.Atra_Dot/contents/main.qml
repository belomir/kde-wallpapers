/*
 * Copyleft by Sergey Roganov, 2013
 * Atra_Dot static wallpaper port to qml
 * distribute under terms of GNU/GPL v.3+
 */
/*
 * TODO: reduce CPU usage
 */

import QtQuick 1.0

Rectangle {
	id: main
	width: 640
	height: 480
	color: "lightgreen"
	property int dots: 3+7*Math.random() // number of planets
	property int pause: 750+1000*Math.random() // time for animation
	
	// signals to connect for children
	signal refresh()
	signal newColor()
	
	onRefresh: {
		//dots = 1+10*Math.random()  // this doesn't work
	}
	
	// the hack to make changes of number of dots not immediate
	// (don't know more accurate way to change number in model yet. suggest need onItemAdd in repeater)
	Behavior on dots {
		SequentialAnimation {
			PauseAnimation { duration: main.pause } // wait for dissapearing of dots
			PropertyAction {} // change immediately
		}
	}
	
	onNewColor: {
		main.color = Qt.hsla(Math.random(), Math.random()*0.5+0.25, Math.random()*0.625+0.125, 1)
	}
	
	Behavior on color { ColorAnimation { duration: 500; easing.type: Easing.InOutCubic } }
	
	MouseArea {
		anchors.fill: parent
		onPressAndHold: {
			newColor();
		}
		onReleased: {
			refresh();
		}
		onDoubleClicked: { // reset
			main.color = "lightgreen";
			main.dots = 2+8*Math.random();
			main.pause = 750+1000*Math.random();
			//refresh();
		}
	}
	
	Timer { // ping some random dot
		running: true
		repeat: true
		interval: main.pause*5*(1+Math.random())
		triggeredOnStart: false
		onTriggered: {
			dots.itemAt(Math.random()*main.dots).refresh()
			interval = main.pause*5*(1+Math.random())
		}
	}
	
	Timer { // refresh current view
		running: true
		repeat: true
		interval: main.pause*60*10*(1+Math.random())
		triggeredOnStart: true
		onTriggered: {
			refresh();
			interval = main.pause*60*10*(1+Math.random())
		}
	}
	
	Timer { // refresh with new color
		running: true
		repeat: true
		interval: main.pause*60*10*(1+Math.random())
		triggeredOnStart: false
		onTriggered: {
			newColor();
			interval = main.pause*60*10*(1+Math.random())
		}
	}
	
	// background
	Rectangle {
		id: background
		anchors.fill: parent
		clip: true
		color: "transparent" // initial value
		smooth: true
		// signal connected to parent
		signal refresh()
		onRefresh: {
			color = Qt.darker(main.color, 1+0.5*Math.random())
		}
		Connections {
			target: main
			onRefresh: background.refresh() // don's work without id
		}
		Behavior on color {
			ColorAnimation {
				duration: main.pause*2 // animate while rings dissapearing and appearing
				easing.type: Easing.InOutCubic
			}
		}
		Image { // halftone gradient image
			id: halftone
			anchors.centerIn: parent
			width: Math.sqrt(Math.pow(main.width,2)+Math.pow(main.height,2))/scale // radius
			height: width
			source: "halftone.png"
			//sourceSize.width: main.width
			//sourceSize.height: main.height // don't know if this really nessesary on modern computers
			fillMode: Image.Tile
			opacity: 0.25
			rotation: 0
			scale: 1 // initial values ↖
			smooth: true
			signal refresh()
			onRefresh: {
				opacity = 0.05+0.25*Math.random()
				rotation += 15-30*Math.random() // make rotation not very big
				scale = 0.5+0.5*Math.random()
			}
			Connections {
				target: main
				onRefresh: halftone.refresh() // don's work without id
			}
			Behavior on rotation {
				NumberAnimation {
					duration: main.pause*2
					easing.type: Easing.InOutCubic
				}
			}
			Behavior on scale {
				NumberAnimation {
					duration: main.pause*2
					easing.type: Easing.InOutCubic
				}
			}
			Behavior on opacity {
				NumberAnimation {
					duration: main.pause*2
					easing.type: Easing.InOutCubic
				}
			}
		}
		// gradients
		// you can turn it off to reduce CPU usage a little
		Rectangle {
			id: gradients
			anchors.centerIn: parent
			width: Math.sqrt(Math.pow(main.width,2)+Math.pow(main.height,2)) // radius
			height: width
			rotation: -45+90*Math.random()
			opacity: 0.125+0.5*Math.random()
			color: "transparent"
			Repeater {
				model: 3+5*Math.random()
				delegate: Rectangle {
					//x: gradients.width/2-width+width*Math.random()
					x: 0
					y: gradients.height*((index+Math.random())/parent.count)///2-height/2*index
					width: gradients.width
					height: gradients.height*(0.1+0.25*Math.random()*index)
					gradient: Gradient {
						GradientStop {
							position: 0.0
							color: "transparent"
						}
						GradientStop {
							position: 0.5
							color: Qt.lighter(main.color, 1+Math.random())
							Behavior on color {
								NumberAnimation {
									duration: main.pause*2
									easing.type: Easing.InOutCubic
								}
							}
						}
						GradientStop {
							position: 1.0
							color: "transparent"
						}
					}
					smooth: true
					signal refresh()
					onRefresh: {
						y = gradients.height*(index/parent.count+Math.random())///2-height/2*index
						height = gradients.height*(0.1+0.25*Math.random()*index)
					}
					Connections {
						target: main
						onRefresh: refresh() // don's work without id
					}
					Behavior on height {
						NumberAnimation {
							duration: main.pause*2
							easing.type: Easing.InOutCubic
						}
					}
					Behavior on y {
						NumberAnimation {
							duration: main.pause*2
							easing.type: Easing.InOutCubic
						}
					}
				}
			}
			clip: true
			smooth: true
			signal refresh()
			onRefresh: {
				opacity = 0.125+0.25*Math.random()
				rotation = -45+90*Math.random()
			}
			Connections {
				target: main
				onRefresh: gradients.refresh() // don's work without id
			}
			Behavior on rotation {
				NumberAnimation {
					duration: main.pause*2
					easing.type: Easing.InOutCubic
				}
			}
			Behavior on opacity {
				NumberAnimation {
					duration: main.pause*2
					easing.type: Easing.InOutCubic
				}
			}
		}
	}
	
	// rings/circles
	Item {
		id: rings
		anchors.fill: parent
		Repeater {
			id: dots
			model: main.dots
			delegate: Item {
				id: index

				property int circles: 2+5*Math.random()
				
				//width: 25+main.width*Math.random()*0.5 // appendix
				width: 0 // initial before it completed
				height: width
				scale: 1
				x: rings.width*Math.random()
				y: rings.height*Math.random()
				//opacity: 0.75*Math.random() // appendix
				opacity: 0 // initial before it completed
				smooth: true
				
				// little hack to make possible simple change model
				Component.onCompleted: {
					opacity = 0.125+0.75*Math.random()
					width = 25+main.width*Math.random()*0.5
					anim.running = true
				}
				
				Repeater { // for single circles in dot
					model: parent.circles
					delegate: Rectangle {
						anchors.centerIn: parent
						//width: 5+parent.width*(index/parent.circles)
						width: 5+parent.width*(index/parent.circles)*(1+Math.random())
						height: width
						radius: width/2
						smooth: true
						opacity: 0.125+0.75*Math.random()
						color: "transparent"
						border.color: "white"
						//border.width: 1+parent.width*(index/parent.circles)*0.125*(Math.random())
						border.width: 1+parent.width*0.25*(0.25+Math.random())/parent.circles
					}
				}
				
				// separate animation for scale, needed in functions like onRefresh
				// because animation don't work inside a function
				SequentialAnimation on scale {
					id: anim
					running: false
					NumberAnimation { // dissolve it simultaniously with opacity
						duration: main.pause
						easing.type: Easing.InOutQuad
						to: 2
					}
					PropertyAction { value: 0 } // immediately change
					NumberAnimation { // and rise from zero
						duration: main.pause
						easing.type: Easing.InOutQuad
						to: 1
					}
				}
				
				signal refresh()
				onRefresh: {
					x = rings.width*Math.random()
					y = rings.height*Math.random()
					width = 25+main.width*Math.random()*0.5
					//scale = 0.05+0.25*Math.random()
					opacity = 0.125+0.75*Math.random()
					anim.running = true // because animation don't work inside function
				}
				Connections {
					target: main
					onRefresh: refresh()
				}
				MouseArea {
					anchors.centerIn: parent
					width: parent.width/8 // area reduced 'cause rings would be rather big
					height: width
					onClicked: {
						refresh();
					}
				}
				Behavior on width {
					SequentialAnimation {
						PauseAnimation { duration: main.pause }
						PropertyAction {}
					}
				}
				Behavior on opacity { // because we want to dissolve make opacity simultaniously with scale
					SequentialAnimation {
						NumberAnimation {
							duration: main.pause
							easing.type: Easing.InOutQuad
							to: 0
						}
						NumberAnimation {
							duration: main.pause
							easing.type: Easing.InOutQuad
						}
					}
				}
				Behavior on x {
					SequentialAnimation {
						PauseAnimation { // wait for ring dissapearing
							duration: main.pause
						}
						PropertyAction {}
					}
				}
				Behavior on y {
					SequentialAnimation {
						PauseAnimation { // wait for ring dissapearing
							duration: main.pause
						}
						PropertyAction {}
					}
				}
			}
		}
	}
}