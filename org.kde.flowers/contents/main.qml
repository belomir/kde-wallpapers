/*
 * Copyleft by Sergey Roganov, 2013
 * distribute under terms of GNU/GPL v.3+
 */

import QtQuick 1.0

Rectangle {
	id: main
	width: 640
	height: 480
	color: "red"
	property int flowers: 3+Math.random()*5
	property color flower_core: "yellow"
	property color flower_petal: "white"
	
	signal refresh()
	signal newColor()
	
	onNewColor: {
		main.color = Qt.hsla(Math.random(), 1, Math.random()*0.5+0.25, 1)
		main.flower_core = Qt.hsla(Math.random(), 1, Math.random()*0.5+0.25, 1)
		main.flower_petal = Qt.hsla(Math.random(), 1, Math.random()*0.5+0.25, 1)
	}
	
	Behavior on color { ColorAnimation { duration: 500; easing.type: Easing.InOutCubic } }
	
	MouseArea {
		anchors.fill: parent
		onPressAndHold: {
			newColor();
		}
		onReleased: {
			refresh();
		}
		onDoubleClicked: {
			main.color = "red"
			main.flower_core = "yellow"
			main.flower_petal = "white"
		}
	}
	
	Timer {
		running: true
		repeat: true
		interval: 1000*60*10
		triggeredOnStart: true
		onTriggered: {
			refresh();
		}
	}
	
	Timer {
		running: true
		repeat: true
		interval: 1000*60*60
		triggeredOnStart: false
		onTriggered: {
			newColor();
		}
	}
	
	Repeater {
		model: main.flowers
		Flower {
			signal refresh()
			x: main.width/2
			y: main.height/2
			smooth: true
			onRefresh: {
				x = Math.random()*(main.width+width)-width
				y = Math.random()*(main.height+height)-height
				width = 50+Math.random()*main.width/5
				rotation = Math.random()*360
				core_color = Qt.darker(main.flower_core, 1+Math.random()/2)
				petal_color = Qt.lighter(main.flower_petal, 1+Math.random()/2)
			}
			Behavior on x { NumberAnimation { duration: 1500; easing.type: Easing.InOutCubic } }
			Behavior on y { NumberAnimation { duration: 1500; easing.type: Easing.InOutCubic } }
			Behavior on width { NumberAnimation { duration: 1500; easing.type: Easing.InOutCubic } }
			Behavior on rotation { NumberAnimation { duration: 1500; easing.type: Easing.InOutCubic } }
			Behavior on core_color { ColorAnimation { duration: 500; easing.type: Easing.InOutCubic } }
			Behavior on petal_color { ColorAnimation { duration: 500; easing.type: Easing.InOutCubic } }
			Connections {
				target: main
				onRefresh: refresh()
			}
			MouseArea {
				anchors.fill: parent
				onClicked: {
					refresh();
				}
			}
		}
	}
	// overrided gradient
	Rectangle {
		id: patina
		anchors.centerIn: parent
		width: parent.width
		height: parent.height
		smooth: true
		gradient: Gradient {
			GradientStop {
				position: 0.0
				color: Qt.hsla(0,0,1,0.1)
			}
			GradientStop {
				position: 1.0
				color: Qt.hsla(0,0,0,0.4)
			}
		}
	}
}