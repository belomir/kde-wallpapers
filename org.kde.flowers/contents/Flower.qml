/*
 * Copyleft by Sergey Roganov, 2013
 * distribute under terms of GNU/GPL v.3+
 */

import QtQuick 1.0

Item {
	id: flower
	width: 250
	height: width
	property int petals: 4+Math.random()*6
	property int rad: 2+Math.random()*6
	property color core_color: "yellow"
	property color petal_color: "red"
	Rectangle {
		id: core
		z: 1
		anchors.centerIn: parent
		width: (flower.width<flower.height?flower.width:flower.height)/flower.rad
		height: width
		color: flower.core_color
		radius: width/2
		smooth: true
	}
	Repeater {
		id: petals
		model: flower.petals
		Item {
			height: (flower.width<flower.height?flower.width:flower.height)/2
			width: height
			smooth: true
			x: flower.width/2-width/2
			y: flower.height/2
			transform: Rotation { origin.x: width/2; origin.y: 0; angle: 360/flower.petals * index }
			Rectangle {
				color: flower.petal_color
// 				border.width: parent.width*0.05
// 				border.color: "black"
				anchors.fill: parent
				radius: width/2
				transform: Scale { origin.x: width/2; xScale: 3/flower.petals }
				smooth: true
			}
		}
	}
}