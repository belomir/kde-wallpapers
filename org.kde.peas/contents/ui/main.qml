﻿/*
 * Copyleft by Sergey Roganov, 2013
 * distribute under terms of GNU/GPL v.3+
 */

import QtQuick 1.1
import Qt.labs.shaders 1.0
// import QtGraphicalEffects 1.0

Rectangle {
	id: main
	width: 640
	height: 480
	color: "red"
	
	property int peases: Math.random()*5+15 // number of peases/bubbles
	
	signal refresh()
	signal newColor()
	
	onNewColor: {
		main.color = Qt.hsla(Math.random(), 1, Math.random()*0.5+0.25, 1)
	}
	
	Behavior on color { ColorAnimation { duration: 500; easing.type: Easing.InOutCubic } }
	
	MouseArea {
		anchors.fill: parent
		onPressAndHold: {
			newColor();
		}
		onReleased: {
			refresh();
		}
		onDoubleClicked: {
			main.color = "red"
		}
	}
	
	Timer {
		running: true
		repeat: true
		interval: 1000*60*10
		triggeredOnStart: true
		onTriggered: {
			refresh();
		}
	}
	
	Repeater {
		id: peass
		model: main.peases
		Rectangle {
			id: index
			signal refresh()
			x: main.width/2
			y: main.height/2
			onRefresh: {
				index.width = (Math.random()*25+1)/100*(main.width<main.height?main.width:main.height)
				index.x = Math.random()*(main.width+width)-width
				index.y = Math.random()*(main.height+height)-height
			}
// 			Rectangle {
// 				anchors.centerIn: parent
// 				width: parent.width*0.75
// 				height: width
// 				color: main.color
// 				radius: width/2
// 			}
			height: width
			radius: width/2
			color: "white"
			smooth: true
			Behavior on x { NumberAnimation { duration: 1500; easing.type: Easing.InOutCubic } }
			Behavior on y { NumberAnimation { duration: 1500; easing.type: Easing.InOutCubic } }
			Behavior on width { NumberAnimation { duration: 1500; easing.type: Easing.InOutCubic } }
			Connections {
				target: main
				onRefresh: refresh()
			}
			MouseArea {
				anchors.fill: parent
				onClicked: {
					refresh();
				}
			}
		}
	}
	
	// overrided gradient
	Rectangle {
		id: patina
		anchors.centerIn: parent
		width: parent.width
		height: parent.height
		smooth: true
		gradient: Gradient {
			GradientStop {
				position: 0.0
				color: Qt.hsla(0,0,1,0.1)
			}
			GradientStop {
				position: 1.0
				color: Qt.hsla(0,0,0,0.4)
			}
		}
	}
	
// 	//an experiment
	ShaderEffectItem {
		id: effect

		property real wave: 0.3
		property real waveOriginX: 0.5
		property real waveOriginY: 0.5
		property real waveWidth: 0.01
		property real aspectRatio: width/height
		//property variant source: 0
		property variant source: ShaderEffectSource {
            sourceItem: peass;
            hideSource: true
        }

		fragmentShader:
			"
			varying mediump vec2 qt_TexCoord0;
			uniform sampler2D source;
			uniform highp float wave;
			uniform highp float waveWidth;
			uniform highp float waveOriginX;
			uniform highp float waveOriginY;
			uniform highp float aspectRatio;

			void main(void)
			{
			mediump vec2 texCoord2 = qt_TexCoord0;
			mediump vec2 origin = vec2(waveOriginX, (1.0 - waveOriginY) / aspectRatio);

			highp float fragmentDistance = distance(vec2(texCoord2.s, texCoord2.t / aspectRatio), origin);
			highp float waveLength = waveWidth + fragmentDistance * 0.25;

			if ( fragmentDistance > wave && fragmentDistance < wave + waveLength) {
				highp float distanceFromWaveEdge = min(abs(wave - fragmentDistance), abs(wave + waveLength - fragmentDistance));
				texCoord2 += sin(1.57075 * distanceFromWaveEdge / waveLength) * distanceFromWaveEdge * 0.08 / fragmentDistance;
			}

			gl_FragColor = texture2D(source, texCoord2.st);
			}
			"
	}
	
}